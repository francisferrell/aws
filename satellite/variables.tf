
variable "name" {
  type = string
  description = "name of the deployment"
}

variable "domain_name" {
  type = string
  description = "domain name to assign to the default API Gateway stage"
}

variable "certificate_name" {
  type = string
  description = <<-EOS
    domain name of the ACM certificate to assign, must match `domain_name`

    For example, `"*.example.com"` where `domain_name = "api.example.com"`
    EOS
}

