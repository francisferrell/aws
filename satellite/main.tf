
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 5.0.0, < 6.0.0"
    }
  }
}

provider "aws" {
  default_tags {
    tags = {
      project = "satellite"
      environment = "production"
    }
  }
}



data "aws_acm_certificate" "this" {
  domain = var.certificate_name
}



module "satellite" {
  source = "gitlab.com/francisferrell/satellite-router-lambda/aws"
  version = "0.2.6"

  name = var.name
  domain_name = var.domain_name
  domain_name_certificate_arn = data.aws_acm_certificate.this.arn

  environment_variables = {
    LOG_SATELLITE = "debug"
    LOG_SATELLITE_LAMBDA = "debug"
  }
}

output "satellite" {
  value = module.satellite
}

