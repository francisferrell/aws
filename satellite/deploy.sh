#!/bin/bash

set -euxo pipefail

VERSION="$1"
PACKAGE_ZIP_URL="https://gitlab.com/api/v4/projects/francisferrell%2Fsatellite/packages/generic/satellite-router-lambda/${VERSION}/satellite-router-lambda-${VERSION}.zip"

rm -f package.zip
curl \
    --fail \
    --silent --show-error \
    --location \
    -o package.zip \
    "$PACKAGE_ZIP_URL"

aws lambda update-function-code \
    --function-name "$LAMBDA_FUNCTION_NAME" \
    --zip-file fileb://package.zip \
    --query 'LastUpdateStatus' \
    --output text \
    ;

aws lambda wait function-updated \
    --function-name "$LAMBDA_FUNCTION_NAME" \
    ;

payload='{"satelliteMeta":"warm"}'

aws lambda invoke \
    --function-name "$LAMBDA_FUNCTION_NAME" \
    --payload "$( echo -n "$payload" | base64 -w 0 )" \
    /dev/null \
    --log-type Tail \
    --query LogResult \
    --output text \
    | base64 -d \
    ;

