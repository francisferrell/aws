
variable "name" {
  type = string
  description = "the repo name"
}

variable "keybase_user" {
  type = string
  description = "keybase username to use to encrypt credentials in the output"
  default = null
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



output "access_key_id" {
  description = "access key ID granting access"
  value = aws_iam_access_key.key.id
}

output "secret_access_key" {
  description = "access secret granting access"
  value = aws_iam_access_key.key.encrypted_secret
}

output "ecr_uri" {
  description = "the URL of the repository"
  value = aws_ecr_repository.this.repository_url
}



resource "aws_ecr_repository" "this" {
  name = var.name
  image_tag_mutability = "MUTABLE"

  tags = var.tags
}



resource "aws_ecr_lifecycle_policy" "this" {
  repository = aws_ecr_repository.this.name

  policy = jsonencode( {
    rules = [
      {
        rulePriority = 1,
        description = "Expire all images",
        selection = {
          tagStatus = "any",
          countType = "sinceImagePushed",
          countUnit = "days",
          countNumber = 1
        },
        action = {
          type = "expire"
        }
      }
    ]
  } )
}



resource "aws_iam_policy" "access" {
  name = "${var.name}-repo-access"
  description = "Grants access to the ${var.name} ECR"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "Auth"
        Effect = "Allow"
        Action = [
          "ecr:GetAuthorizationToken",
        ]
        Resource = [
          "*",
        ]
      },

      {
        Sid = "PushPull"
        Effect = "Allow"
        Action = [
          "ecr:BatchCheckLayerAvailability",
          "ecr:BatchGetImage",
          "ecr:GetDownloadUrlForLayer",
          "ecr:CompleteLayerUpload",
          "ecr:InitiateLayerUpload",
          "ecr:PutImage",
          "ecr:UploadLayerPart",
        ]
        Resource = [
          aws_ecr_repository.this.arn,
        ]
      },
    ]
  } )

  tags = var.tags
}



resource "aws_iam_user" "user" {
  name = var.name
  tags = var.tags
}

resource "aws_iam_access_key" "key" {
  user = aws_iam_user.user.name
  pgp_key = var.keybase_user == null ? "" : "keybase:${var.keybase_user}"
}

resource "aws_iam_user_policy_attachment" "policy_attachment" {
  user = aws_iam_user.user.name
  policy_arn = aws_iam_policy.access.arn
}

