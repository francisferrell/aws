
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.64.0"
    }
  }

  backend "s3" {
    profile = "terraform"
    region = "us-east-1"
    bucket = "terraform-francisferrell"
    key = "misc.tfstate"
    dynamodb_table = "terraform-francisferrell"
  }
}

provider "aws" {
  profile = "terraform"
  region = "us-east-1"
}



variable "backup_bucket" {
  type = string
  description = "name of the S3 bucket for backups"
}

variable "backup_users" {
  type = list(string)
  description = "list of users who will back up to the bucket"
  default = []
}

variable "domain_name" {
  type = string
  description = "domain name for Let's Encrypt API access"
}

variable "keybase_user" {
  type = string
  description = "keybase username to use to encrypt IAM secret access keys"
  default = null
}



output "letsencrypt_access_key_id" {
  description = "the letsencrypt user's access key id"
  value = module.letsencrypt-api-user.access_key_id
}

output "letsencrypt_secret_access_key" {
  description = "the letsencrypt user's access key id"
  value = module.letsencrypt-api-user.secret_access_key
}

output "backup_credentials" {
  description = "credentials for performing backups"
  value = module.s3-backups.credentials
}



module "letsencrypt-api-user" {
  source = "./letsencrypt-api-user"

  name = "letsencrypt-api"
  domain_name = var.domain_name
  keybase_user = var.keybase_user

  tags = {
    application = "letsencrypt"
  }
}



module "s3-backups" {
  source = "./s3-backups"

  name = var.backup_bucket
  keybase_user = var.keybase_user
  users = var.backup_users

  tags = {
    application = "backups"
  }
}



output "ci_example_access_key_id" {
  description = "the ci-example user's access key id"
  value = module.ci-example.access_key_id
}

output "ci_example_secret_access_key" {
  description = "the ci-example user's access key id"
  value = module.ci-example.secret_access_key
}

output "ci_example_uri" {
  description = "the ci-example ECR URI"
  value = module.ci-example.ecr_uri
}



module "ci-example" {
  source = "./ci-example"

  name = "ci-example"
  keybase_user = var.keybase_user

  tags = {
    application = "francisferrell/ci"
  }
}

