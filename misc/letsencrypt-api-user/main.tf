
variable "name" {
  type = string
  description = "the user name"
}

variable "domain_name" {
  type = string
  description = "domain name for Let's Encrypt API access"
}

variable "keybase_user" {
  type = string
  description = "keybase username to use to encrypt the secret_access_key output"
  default = null
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



output "access_key_id" {
  description = "the user's access key id"
  value = aws_iam_access_key.key.id
}

output "secret_access_key" {
  description = "the user's access key id"
  value = aws_iam_access_key.key.encrypted_secret
}



data "aws_route53_zone" "zone" {
  name = var.domain_name
}



resource "aws_iam_user" "user" {
  name = var.name
  tags = var.tags
}

resource "aws_iam_access_key" "key" {
  user = aws_iam_user.user.name
  pgp_key = var.keybase_user == null ? "" : "keybase:${var.keybase_user}"
}



resource "aws_iam_policy" "policy" {
  name = "${var.name}-policy"
  description = "grants ${var.name} access to perform DNS-01 validation under ${var.domain_name}"
  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "Route53List"
        Effect = "Allow"
        Action = [
          "route53:ListHostedZones",
        ]
        Resource = [
          "*",
        ]
      },
      {
        Sid = "Route53ManageRecords"
        Effect = "Allow"
        Action = [
          "route53:ChangeResourceRecordSets",
          "route53:GetHostedZone",
          "route53:ListResourceRecordSets",
        ]
        Resource = [
          "arn:aws:route53:::hostedzone/${data.aws_route53_zone.zone.zone_id}"
        ]
      },
    ] # end Statement[]
  } )
  tags = var.tags
}

resource "aws_iam_user_policy_attachment" "policy_attachment" {
  user = aws_iam_user.user.name
  policy_arn = aws_iam_policy.policy.arn
}

