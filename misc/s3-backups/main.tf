
variable "name" {
  type = string
  description = "the bucket name"
}

variable "keybase_user" {
  type = string
  description = "keybase username to use to encrypt credentials in the output"
  default = null
}

variable "users" {
  type = list(string)
  description = "list of users who will perform backups"
  default = []
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



output "credentials" {
  description = "credentials for performing backups"
  value = { for user in var.users:
    user => {
      access_key_id = aws_iam_access_key.key[user].id
      access_secret = aws_iam_access_key.key[user].encrypted_secret
    }
  }
}



resource "aws_s3_bucket" "bucket" {
  bucket = var.name
  acl = "private"

  server_side_encryption_configuration {
    rule {
      bucket_key_enabled = false
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = var.tags
}



resource "aws_s3_bucket_public_access_block" "private" {
  bucket = aws_s3_bucket.bucket.id
  block_public_acls = true
  block_public_policy = true
  ignore_public_acls = true
  restrict_public_buckets = true
}



resource "aws_iam_policy" "read_write_policy" {
  for_each = toset( var.users )

  name = "${aws_s3_bucket.bucket.id}-s3-read-write-access-${each.value}"
  description = "grants read/write access to the ${aws_s3_bucket.bucket.id} s3 bucket for ${each.value}"
  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ListBucket"
        Effect = "Allow"
        Action = [
          "s3:ListBucket",
        ]
        Resource = [
          aws_s3_bucket.bucket.arn,
        ]
      },
      {
        Sid = "ManageObjects"
        Effect = "Allow"
        Action = [
          "s3:DeleteObject",
          "s3:GetObject",
          "s3:PutObject",
        ]
        Resource = [
          "${aws_s3_bucket.bucket.arn}/${each.value}/*",
        ]
      },
    ] # end Statement[]
  } )
  tags = var.tags
}



resource "aws_iam_user" "user" {
  for_each = toset( var.users )

  name = "backups-${each.value}"
  tags = var.tags
}

resource "aws_iam_access_key" "key" {
  for_each = toset( var.users )

  user = aws_iam_user.user[each.value].name
  pgp_key = var.keybase_user == null ? "" : "keybase:${var.keybase_user}"
}

resource "aws_iam_user_policy_attachment" "policy_attachment" {
  for_each = toset( var.users )

  user = aws_iam_user.user[each.value].name
  policy_arn = aws_iam_policy.read_write_policy[each.value].arn
}

